from tensorflow.compat.v1.lite import TFLiteConverter
# Load the MobileNet saved tf.keras model.
converter = TFLiteConverter.from_keras_model_file('saved_model.h5', input_shapes={"input_1":[1,280,280,3]})
tflite_model = converter.convert()
open("converted_model.tflite", "wb").write(tflite_model)

