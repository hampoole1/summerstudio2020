from tensorflow.keras.datasets import mnist
import numpy as np

(x_train, y_train), (x_test, y_test) = mnist.load_data()
# Reshape data
x_train = x_train.reshape((x_train.shape[0], x_train.shape[1], x_train.shape[2], 1)).astype('float32')
x_train = (x_train - 127.5) / 127.5

num_examples = x_train.shape[0]

gan = GAN(x_train)
